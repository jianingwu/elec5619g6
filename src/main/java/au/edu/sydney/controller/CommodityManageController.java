package au.edu.sydney.controller;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Commodity;
import au.edu.sydney.service.CommodityService;

@Controller
@Transactional
public class CommodityManageController {
	//git rys
	protected final Log logger=LogFactory.getLog(getClass());
	@Autowired
	private CommodityService commodityService;
	
	@RequestMapping(value = "/showCommodityForm")
	public ModelAndView showCommodity(Model theModel) {
		//CommodityService commodityService=new CommodityService();
		List<Commodity> commoditiesList=commodityService.showCommodities("");
		theModel.addAttribute("commodities",commoditiesList);
		String msg="msg";
		return new ModelAndView("commodity-manage","msg",msg);
	}
	
	@RequestMapping(value="/addCommodity", method=RequestMethod.POST)
	public ModelAndView AddCommodity(Model theModel, HttpSession session, 
			@ModelAttribute("commodity") Commodity commodity) {
		String msg="Successed add Commodity";
		if(commodity!=null)
		{
			commodityService.saveCommodity(commodity);
			return new ModelAndView("redirect:/showCommodityForm","msg",msg);
		}else
		{
			msg="Something worng";
			return new ModelAndView("add-commodity","msg",msg);
		}
	}
	@RequestMapping(value="/showAddCommodity",method=RequestMethod.GET)
	public String showAddCommodity(Model theModel) {
		return "add-commodity";
	}
	@RequestMapping(value="/updateCommodity",method=RequestMethod.POST)
	public ModelAndView UpdateCommodity(Model theModel, HttpSession session,
			@ModelAttribute("commodity") Commodity commodity,@ModelAttribute("id") long commodityId) {
		//logger.info("this is controller"+commodity.toString());
		commodityService.updateCommodity(commodity, commodityId);
		System.out.println(commodity);
		return new ModelAndView("redirect:/showCommodityForm");
	}

	@RequestMapping(value="/deleteCommodity",method=RequestMethod.GET)
	public String DeleteCommodity(@RequestParam("")long commodityId,
			Model theModel, HttpSession session) {
		commodityService.deleteCommodity(commodityId);
		return "redirect:/showCommodityForm";
	}
	
	@RequestMapping(value="/showCommodityFormUpdate")
	public String showCommodityFormUpdate(@RequestParam("commodityId") int commodityId,
			Model theModel, HttpSession session){
		Commodity theCommodity=commodityService.getCommodity(commodityId);
		if (theCommodity!=null) {
		theModel.addAttribute("id", theCommodity.getId());
		theModel.addAttribute("com_name", theCommodity.getCom_name());
		theModel.addAttribute("com_amount", theCommodity.getCom_amount());
		theModel.addAttribute("com_price", theCommodity.getCom_price());
		theModel.addAttribute("com_discount", theCommodity.getCom_discount());
		theModel.addAttribute("com_type", theCommodity.getCom_type());
		theModel.addAttribute("com_describe", theCommodity.getCom_describe());
		theModel.addAttribute("com_pic", theCommodity.getCom_pic());
		theModel.addAttribute("com_size", theCommodity.getCom_size());
		theModel.addAttribute("com_saled_amount", theCommodity.getCom_saled_amount());
		theModel.addAttribute("com_color", theCommodity.getCom_color());
		theModel.addAttribute("com_rank_total", theCommodity.getCom_rank_total());
		theModel.addAttribute("com_rank_num", theCommodity.getCom_rank_num());
		theModel.addAttribute("if_deleted", theCommodity.getIf_deleted());
		return "updata-commodity";
		} else {
			return "redirect:/showCommodityForm";
		}
	}
//	@RequestMapping(value="/showCommodityDetail", method=RequestMethod.GET)
//	public ModelAndView showCommodityDetail(Model theModel, HttpSession session,
//			@ModelAttribute("Commodity") Commodity commodity) {
//		if(commodity!=null)
//		{
//			logger.info("already get commodity");
//			List<Commodity> commodities=commodityService.showCommodities("");
//			Map<String,Object>myModel =new HashMap<String,Object>();
//			myModel.put("commodities", commodities);
//			logger.info("already get commodity"+commodities);
//			return new ModelAndView("add-commodity","model",myModel);	
//		}else
//		{
//			return new ModelAndView("main-menu");
//		}
//		
//	}

}
