package au.edu.sydney.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;
import au.edu.sydney.service.UserService;

@Controller
//@SessionAttributes("login")
public class LoginController {

	@Autowired
	private UserService userService;

	private static Logger log = Logger.getLogger(LoginController.class);

	// show the log in form
	@RequestMapping("/showLogInForm")
	public String showLogInForm() {
		return "login-form";
	}

	@ModelAttribute("login")
	public Login getLogin() {
		return new Login();
	}

	// check if the user information are valid or not
	@RequestMapping(value = "/showHomePage", method = RequestMethod.GET)
	public ModelAndView showHome(HttpServletRequest request, Model theModel, HttpSession session)
			throws ServletException, IOException {
		Login u = (Login) session.getAttribute("login");
		User user = userService.findUserByName(u.getUserName());
		String msg = "";
		boolean isValid = false;
		if (user != null) {
			msg = "Hi " + user.getUserName() + " !";
			isValid = true;
			List<UserDetail> userDetailList = userService.showUserDetail(user.getUserName());
			theModel.addAttribute("Detail", userDetailList);
			return new ModelAndView("user-role", "msg", msg);
	
		} else {
			return new ModelAndView("login-form", "msg", msg);
		}

	}

	// check if the user information are valid or not
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView validateUser(@ModelAttribute("login") Login login, HttpServletRequest request, Model theModel,
			BindingResult result) throws ServletException, IOException {

		User user = userService.validateUser(login);
		String msg = "";

		boolean isValid = false;
		if (user != null) {
			msg = "Welcome " + user.getUserName() + " !";
			isValid = true;
			log.info("Is user valid ?= " + isValid);
			request.getSession().setAttribute("login", login);
			return new ModelAndView("redirect:/showHomePage", "msg", msg);

		} else {
			msg = "Invalid credentials ! Try Again";
			return new ModelAndView("login-form", "msg", msg);
		}

	}

	// check if the user information are valid or not
	// @RequestMapping(value = "/login", method = RequestMethod.POST)
	// public ModelAndView validateUser(@ModelAttribute("login") Login login,
	// HttpServletRequest request,Model theModel,
	// BindingResult result) throws ServletException, IOException {
	//
	// User user = userService.validateUser(login);
	// String msg = "";
	//
	// boolean isValid = false;
	// if (user != null) {
	// msg = "Welcome " + user.getUserName() + " !";
	// isValid = true;
	// log.info("Is user valid ?= " + isValid);
	// request.getSession().setAttribute("login", login);
	// List<UserDetail> userDetailList =
	// userService.showUserDetail(user.getUserName());
	// theModel.addAttribute("Detail", userDetailList);
	// return new ModelAndView("user-role", "msg", msg);
	// } else {
	// msg = "Invalid credentials ! Try Again";
	// return new ModelAndView("login-form", "msg", msg);
	// }
	//
	// }
	//
	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request) {
//		request.getSession().invalidate();
		
		request.getSession().removeAttribute("login");
		return "login-form";
	}

}
