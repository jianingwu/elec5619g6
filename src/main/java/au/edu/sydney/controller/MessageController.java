package au.edu.sydney.controller;

import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.Message;
import au.edu.sydney.domain.User;
import au.edu.sydney.service.MessageService;
import au.edu.sydney.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MessageController {
    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;
    private String messagecontent;
    private String sendtoname;
    private String messageTitle;
    private Integer deleteMessageId;

    public MessageService getMessageService() {
        return messageService;
    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/sendmessage")
    public ModelAndView sendMessage(Model theModel, HttpSession session) {
        Login u = (Login) session.getAttribute("login");
        User user = userService.findUserByName(u.getUserName());
        List<User> userList = userService.findAllUsers();
        if (user != null) {
            theModel.addAttribute("userList", userList);
            theModel.addAttribute("userEmailOfSender", user.getEmail());
            theModel.addAttribute("messageSenderName", user.getUserName());
            return new ModelAndView("send-message");
        } else {
            return new ModelAndView("login-form");
        }
    }

    public String getMessagecontent() {
        return messagecontent;
    }

    public void setMessagecontent(String messagecontent) {
        this.messagecontent = messagecontent;
    }

    public String getSendtoname() {
        return sendtoname;
    }

    public void setSendtoname(String sendtoname) {
        this.sendtoname = sendtoname;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    @RequestMapping(value = "/sendNewMessage", method = RequestMethod.POST)
    public ModelAndView saveMessage(Model theModel, String sendtoname, String messagecontent,
                                    String messageTitle, HttpSession session) {
        Login u = (Login) session.getAttribute("login");
        User sender = userService.findUserByName(u.getUserName());
        User receiver = userService.findUserByName(sendtoname);
        if (receiver != null && messagecontent != null && messageTitle != null && sendtoname != null) {
            Message message = new Message(sender.getId(), receiver.getId(), messageTitle, messagecontent, sender.getUserName(), receiver.getUserName());
            messageService.saveMessage(message);
        }
        List<User> userList = userService.findAllUsers();
        theModel.addAttribute("userList", userList);
        theModel.addAttribute("userEmailOfSender", sender.getEmail());
        theModel.addAttribute("messageSenderName", sender.getUserName());
        setSendtoname("");
        setMessagecontent("");
        setMessageTitle("");
        return new ModelAndView("send-message");
    }

    @RequestMapping(value = "deleteMessage")
    public void deleteMessage(Integer deleteMessageId) {
        messageService.deleteMessageById(deleteMessageId);
    }

    @RequestMapping("/checkmessages")
    public ModelAndView checkMessage(Model theModel, HttpServletRequest request, HttpSession session) {
        Login u = (Login) session.getAttribute("login");
        User user = userService.findUserByName(u.getUserName());
        String deleteId = request.getParameter("id");
        if (user != null) {
            if (deleteId != null) {
                messageService.deleteMessageById(Integer.parseInt(deleteId));
            }
            List<Message> messageList = messageService.getMessagesByRecieverName(u.getUserName());
            theModel.addAttribute("userEmailOfSender", user.getEmail());
            theModel.addAttribute("messageSenderName", user.getUserName());
            theModel.addAttribute("Messages", messageList);
            return new ModelAndView("check-messages");
        } else {
            return new ModelAndView("login-form");
        }
    }
}

