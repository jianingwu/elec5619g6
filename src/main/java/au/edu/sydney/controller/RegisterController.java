package au.edu.sydney.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.User;
import au.edu.sydney.service.UserService;

@Controller
public class RegisterController {
	
	//need to inject our user service
	@Autowired
	private UserService userService;
	
	//add an initbinder ... to convert trim input strings
	//remove leading and trailing whitespaces
	//resolve issue for our validation
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor stringTrimmerEditor =new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
	
	//show the register user form
	@RequestMapping("/showRegisterForm")
	public String showRegisterForm(Model theModel) {
		//create model attribute to bind form data
		User theUser=new User();	
		//add the users to the model
		theModel.addAttribute("user",theUser);	
		return "register-form";
	}
	
	//save the form data to the database
	@RequestMapping("/saveUser")
	public ModelAndView saveUser(@Valid @ModelAttribute("user") User theUser,BindingResult theBindingResult) {
		//System.out.println(theUser.getUserName());
		//save the user using service
		String msg="";
		if (userService.findByUserName(theUser.getUserName()) != null) {
			System.out.println("user exists");
			msg="User Name Exists, Try Another one";
			return new ModelAndView("register-form","msg",msg);
		}		
		if(theBindingResult.hasErrors()){
			msg="All the fields are required";
			return new ModelAndView("register-form","msg",msg);
		}else {
			msg="";
			userService.saveUser(theUser);
			return new ModelAndView("login-form","msg",msg);
		}
	}
	
	// new a controller method to read form data and add data to the model
//	@RequestMapping("/processFormVersionTwo")
//	public String addUser(HttpServletRequest request, Model model) {
//		// read the request paramenter from the HTML form
//		String theName = request.getParameter("userName");
//		// conver the data to all caps
//		theName = theName.toUpperCase();
//		// create the message
//		String result = "Hello! " + theName;
//		// add message to the model
//		model.addAttribute("message", result);
//		return "helloworld";
//	}
}
