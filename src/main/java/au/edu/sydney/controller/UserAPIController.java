package au.edu.sydney.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.edu.sydney.domain.User;
import au.edu.sydney.service.UserService;

@RestController
public class UserAPIController {

//	private final User user;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/test")
	public String sayHello() {
		return "hello";
	}
	//To Runzhe, you can only call this API, just type sydney/listAllUsers you will get a list of objects
	@RequestMapping(value = "/listAllUsers", method = RequestMethod.GET)
	public List<User> getUsers(){
		List<User> users=userService.findAllUsers();
		if (users.isEmpty()) {
			return null;
		}
		return users;
	}
	
	
	@RequestMapping(value = "/test1", method = RequestMethod.GET)
	public List<User> getUsers2(){
		List<User> theusers=new ArrayList<>();
		theusers.add(new User("user", "null12fs","1223"));
		theusers.add(new User("usedd", "nuddll12fs","12dd23"));
		return theusers;
	}
	

}
