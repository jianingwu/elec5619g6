package au.edu.sydney.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Commodity;
import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;
import au.edu.sydney.service.CommodityService;
import au.edu.sydney.service.UserDetailService;
import au.edu.sydney.service.UserService;

@Controller
@SessionAttributes("login")
public class MainPageController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserDetailService userDetailService;
	@Autowired
	private CommodityService commodityService;
	
	@RequestMapping(value="/showMainPage")
	public ModelAndView showMainPage(Model theModel, HttpSession session){
		Login u=(Login) session.getAttribute("login");
		User theUser=userService.findUserByName(u.getUserName());
		List<Commodity> commoditiesList=commodityService.showCommodities("");
		String msg="";
		if(theUser!=null) {
			UserDetail userDetail=userService.showUserDetail(u.getUserName()).get(userService.showUserDetail(u.getUserName()).size()-1);
			theModel.addAttribute("user",theUser);
			theModel.addAttribute("userDetail", userDetail);
			theModel.addAttribute("commodities",commoditiesList);
			return new ModelAndView("main-page","msg",msg);
		}else
		{
			msg="Can not find user, need to login again";
			return new ModelAndView("main-menu");
		}
	}
	
	
}
