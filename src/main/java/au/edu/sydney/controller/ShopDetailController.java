package au.edu.sydney.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Commodity;
import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.ShopDetail;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;
import au.edu.sydney.service.CommodityService;
import au.edu.sydney.service.UserDetailService;
import au.edu.sydney.service.UserService;

@Controller
@SessionAttributes("login")
public class ShopDetailController {
	protected final Log logger=LogFactory.getLog(getClass());
	@Autowired
	private UserService userService;
	@Autowired
	private UserDetailService userDetailService;
	@Autowired
	private CommodityService commodityService;
	
	@ModelAttribute("shopDetail")
	public ShopDetail getShopDetail() {
		return new ShopDetail();
	}
	
	@RequestMapping(value = "/checkCommodityDetail", method = RequestMethod.POST)
	public ModelAndView checkCommodityDetail(@ModelAttribute("shopDetail") ShopDetail shopDetail,
			 HttpServletRequest request, Model theModel,BindingResult result,HttpSession session) {
		String msg="";
		logger.info("this is ShopDetail"+shopDetail.toString());
		if (shopDetail!=null) {	
			Commodity commodity=commodityService.getCommodity(shopDetail.getCommodity_id());
			commodity.setId(shopDetail.getCommodity_id());
			Login u=(Login) session.getAttribute("login");
			User theUser=userService.findUserByName(u.getUserName());
			UserDetail userDetail=userService.showUserDetail(u.getUserName()).get(userService.showUserDetail(u.getUserName()).size()-1);
			theModel.addAttribute("user",theUser);
			theModel.addAttribute("userDetail", userDetail);
			theModel.addAttribute("commodity",commodity);
			return new ModelAndView("shop-detail","msg",msg);
		} else {
			return new ModelAndView("main-page");
		}
	}
	
	@RequestMapping(value = "/buynow", method = RequestMethod.POST)
	public ModelAndView buynow(@ModelAttribute("shopDetail") ShopDetail shopDetail,
			 HttpServletRequest request, Model theModel,BindingResult result,HttpSession session) {
		String msg="";
		logger.info("this is ShopDetail"+shopDetail.toString());
		if (shopDetail!=null) {	
			Commodity commodity=commodityService.getCommodity(shopDetail.getCommodity_id());
			commodity.setId(shopDetail.getCommodity_id());
			Login u=(Login) session.getAttribute("login");
			User theUser=userService.findUserByName(u.getUserName());
			UserDetail userDetail=userService.showUserDetail(u.getUserName()).get(userService.showUserDetail(u.getUserName()).size()-1);
			theModel.addAttribute("user",theUser);
			theModel.addAttribute("userDetail", userDetail);
			theModel.addAttribute("commodity",commodity);
			return new ModelAndView("order","msg",msg);
		} else {
			return new ModelAndView("main-page");
		}
	}
}
