package au.edu.sydney.controller;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.Order;
import au.edu.sydney.domain.Post;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;
import au.edu.sydney.service.PostService;
import au.edu.sydney.service.UserDetailService;
import au.edu.sydney.service.UserService;

@Controller
@Transactional
@SessionAttributes("login")
public class PostController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PostService postService;
	
	@RequestMapping(value="/showPostPage")
	public ModelAndView showPostPage(Model theModel, HttpSession session) {
		Login u=(Login) session.getAttribute("login");
		User theUser=userService.findUserByName(u.getUserName());
		List<Post> postList=postService.showPost("");
		Collections.reverse(postList);
		String msg="";
		String name="";
		if(theUser!=null){
			UserDetail userDetail=userService.showUserDetail(u.getUserName()).get(userService.showUserDetail(u.getUserName()).size()-1);
			name=userDetail.getFirstName()+userDetail.getLastName();
			theModel.addAttribute("user",theUser);
			theModel.addAttribute("userDetail", userDetail);
			theModel.addAttribute("posts",postList);
			theModel.addAttribute("name", name);
			return new ModelAndView("post-page","msg",msg);
		}else {
			msg="Can not find user, need to login again";
			return new ModelAndView("main-menu");
		}
	}
	
	@RequestMapping(value="/addPost", method=RequestMethod.POST)
	public ModelAndView addPost(Model theModel, HttpSession session, 
			@ModelAttribute("post") Post post) {
		String msg="success add Post";
		if(post!=null) {
			String now=(new Date()).toString();
			post.setPost_time(now);
			postService.savePost(post);
			return new ModelAndView("redirect:/showPostPage");
			
		} else {
			return new ModelAndView("main-menu");
		}
	}
}
