package au.edu.sydney.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Commodity;
import au.edu.sydney.domain.Order;
import au.edu.sydney.service.CommodityService;
import au.edu.sydney.service.OrderService;


@Controller
@Transactional
@SessionAttributes("login")
public class OrderController {

	protected final Log logger=LogFactory.getLog(getClass());
	@Autowired
	private CommodityService commodityService;

	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/addOrder", method=RequestMethod.POST)
	public ModelAndView addOrder(Model theModel, HttpSession session, 
			@ModelAttribute("order") Order order) {
		String msg="Success add Order";
		
		if(order!=null)
		{
			Commodity commodity=commodityService.getCommodity(order.getCommodity_id());
			if (!commodityService.changeNumber((int)order.getCom_amount(), commodity, order.getCommodity_id())) {
				return new ModelAndView("main-menu");
			}
			String now=(new Date()).toString();
			order.setOrder_time(now);
//			System.out.println(order);
//			logger.info("this is controller"+order.toString());
			orderService.createOrder(order);
			
			return new ModelAndView("redirect:/showMainPage");
		} else {
			return new ModelAndView("main-menu");
		}
	}
}
