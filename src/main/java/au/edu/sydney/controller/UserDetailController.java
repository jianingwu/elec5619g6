package au.edu.sydney.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;
import au.edu.sydney.service.UserDetailService;
import au.edu.sydney.service.UserService;

@Controller
@SessionAttributes("login")
public class UserDetailController {

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/showUserDetail", method = RequestMethod.GET)
	public ModelAndView showAddUserDetail(Model theModel, HttpSession session,
			@ModelAttribute("userDetail") UserDetail userDetail) {

		Login u = (Login) session.getAttribute("login");
		User theUser = userService.findUserByName(u.getUserName());

		if (theUser != null) {

			List<UserDetail> userDetailList = userService.showUserDetail(theUser.getUserName());

			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("Detail", userDetailList);
			return new ModelAndView("add-user-detail", "model", myModel);

		} else {
			return new ModelAndView("main-menu");
		}
	}

	@RequestMapping(value = "/addUserDetail", method = RequestMethod.POST)
	public ModelAndView AddUserDetail(Model theModel, HttpSession session,
			@ModelAttribute("userDetail") UserDetail userDetail) {

		Login u = (Login) session.getAttribute("login");
		User theUser = userService.findUserByName(u.getUserName());
		String msg = "";
		if (theUser != null) {
			msg = "Succeed Saving user detail , " + theUser.getUserName() + " !";
			userService.addUserDetail(userDetail, theUser.getUserName());
			return new ModelAndView("redirect:/showHomePage", "msg", msg);

		} else {
			msg = "Something wrong";
			return new ModelAndView("add-user-detail", "msg", msg);
		}
	}

	@RequestMapping(value = "/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("userDetailId") int userDetailId, Model theModel,
			HttpSession session) {
		Login u = (Login) session.getAttribute("login");
		User theUser = userService.findUserByName(u.getUserName());
		if (theUser != null) {
			// get the userDetail from the database
			UserDetail theuserDetail = userDetailService.getUserDetail(userDetailId);
			// set userDetail as a model attribute to pre-populate the form
			theModel.addAttribute("id", theuserDetail.getId());
			theModel.addAttribute("firstName", theuserDetail.getFirstName());
			theModel.addAttribute("lastName", theuserDetail.getLastName());
			theModel.addAttribute("email", theuserDetail.getEmail());
			theModel.addAttribute("phoneNumber", theuserDetail.getPhoneNumber());
			theModel.addAttribute("street", theuserDetail.getStreet());
			theModel.addAttribute("postcode", theuserDetail.getPostcode());
			theModel.addAttribute("city", theuserDetail.getCity());
			theModel.addAttribute("suburb", theuserDetail.getSuburb());
			theModel.addAttribute("state", theuserDetail.getState());
			// send over to our form
			return "update-user-detail";
		} else {
			return "redirect:/showHomePage";
		}

	}

	@RequestMapping(value = "/updateUserDetail", method = RequestMethod.POST)
	public ModelAndView UpdateUserDetail(Model theModel, HttpSession session,
			@ModelAttribute("userDetail") UserDetail userDetail) {

		Login u = (Login) session.getAttribute("login");
		User theUser = userService.findUserByName(u.getUserName());
		System.out.println(userDetail);

		userService.updateUserDetail(userDetail, theUser.getUserName());

		return new ModelAndView("redirect:/showHomePage");

	}
	
	@RequestMapping(value = "/deleteUserDetail", method = RequestMethod.GET)
	public String deleteUserDetail(@RequestParam("userDetailId") int userDetailId,Model theModel,
			HttpSession session) {
		Login u = (Login) session.getAttribute("login");
		User theUser = userService.findUserByName(u.getUserName());
		//delete the user detail
		userService.deleteUserDetail(userDetailId, theUser.getUserName());
		
		return "redirect:/showHomePage";
	}
	
}
