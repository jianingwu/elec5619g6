package au.edu.sydney.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Post;

@Repository(value="postDao")
public class PostDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void savePost(Post post) {
		Session currentSession=this.sessionFactory.getCurrentSession();
		
		currentSession.save(post);
	}
	
	public Post getPost(long postId) {
		Session currentSession=sessionFactory.getCurrentSession();
		Criteria criteria=currentSession.createCriteria(Post.class);
		criteria.add(Restrictions.eq("id", postId));
		Post post=(Post) criteria.uniqueResult();
		if(post !=null) {
			System.out.println("commodity found");
			return post;
		}else
		{
			return null;
		}
	}
	public void set_user_name(Post post,String first_name, String second_name) {
		String name;
		name=first_name+second_name;
		post.setPost_user_name(name);
	}
public List<Post> showPost(String keyword)
{
	if(keyword=="")
	{
		Session currentSession=sessionFactory.openSession();
		Transaction tx=currentSession.beginTransaction();
		String sql="from Post";
		Query post=currentSession.createQuery(sql);
		System.out.println(post.toString());
		List<Post> postList=(List<Post>) post.list();
		return postList;
	}else
	{
		return null;
	}
}



}
