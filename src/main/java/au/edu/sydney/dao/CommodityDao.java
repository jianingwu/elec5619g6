package au.edu.sydney.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Commodity;

@Repository(value = "commodityDao")
public class CommodityDao {

	protected final Log logger=LogFactory.getLog(getClass());
	
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void saveCommodity(Commodity commodity) {
		Session currentSession=this.sessionFactory.getCurrentSession();
		
		currentSession.save(commodity);
	}
	
	public Commodity getCommodity(long commodityId) {
		Session currentSession=sessionFactory.getCurrentSession();
		Criteria criteria =currentSession.createCriteria(Commodity.class);
		criteria.add(Restrictions.eq("id", commodityId));
		Commodity commodity=(Commodity) criteria.uniqueResult();
		if(commodity !=null){
			System.out.println("commodity found");
			return commodity;
		}else
		{
			return null;
		}
	}
	public List<Commodity> showCommodities(String keyword)
	{
		if(keyword=="")
		{
			Session currentSession=sessionFactory.openSession();
			Transaction tx=currentSession.beginTransaction();
			String sql="from Commodity";
			Query commodity =currentSession.createQuery(sql);
			System.out.println(commodity.toString());
			List<Commodity> commodityList=(List<Commodity>) commodity.list();
			return commodityList;
		}else
		{
			return null;
		}
	}

	public void updateCommodity(Commodity commodity,long commodityId) {
		//logger.info("this is dao"+commodity);
		//logger.info("this is dao"+commodityId);
		if(commodity!=null) {
			Session currentSession=sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			Commodity oldCommodity=(Commodity)currentSession.get(Commodity.class, commodityId);
			oldCommodity.refreshCommodity(commodity,commodityId);
			currentSession.clear();
			currentSession.update(oldCommodity);
			currentSession.flush();
		}
	}
	public void deleteCommodity(long commodityId) {
		Commodity oldCommodity=getCommodity(commodityId);
		if(oldCommodity!=null){
			Session currentSession=sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			Query theQuery=currentSession.createQuery("delete from Commodity where id=:commodityId");
			theQuery.setParameter("commodityId", commodityId);
			theQuery.executeUpdate();
		}
	}
	public boolean changeNumber(int num, Commodity commodity,long commodityId){
		
		if(commodity!=null) {
			if(num>commodity.getCom_amount()) {
				return false;
			}
			Session currentSession=sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			Commodity oldCommodity=(Commodity)currentSession.get(Commodity.class, commodityId);
			oldCommodity.refreshCommodity(commodity,commodityId);
			oldCommodity.setCom_amount(oldCommodity.getCom_amount()-num);
			oldCommodity.setCom_saled_amount(oldCommodity.getCom_saled_amount()+num);
			currentSession.clear();
			currentSession.update(oldCommodity);
			currentSession.flush();
			return true;
		}else {
			return false;
		}
	}
	public void addCommodity(Commodity commodity)
	{
		Session currentSession=sessionFactory.getCurrentSession();
		currentSession.save(commodity);
	}
}
