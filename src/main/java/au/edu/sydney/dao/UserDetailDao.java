package au.edu.sydney.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;

@Repository(value = "userDetailDao")
public class UserDetailDao {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveUserDetail(UserDetail userDetail) {
		// get current hibernate session
		Session currentSession = this.sessionFactory.getCurrentSession();
	
		currentSession.save(userDetail);
	}

	public UserDetail getUserDetail(int userDetailId) {
		//get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria criteria = currentSession.createCriteria(UserDetail.class);
		criteria.add(Restrictions.eq("id", userDetailId));
		UserDetail userDetail = (UserDetail) criteria.uniqueResult();
		if (userDetail != null) {
			System.out.println("UserDetail found");
			return userDetail;
		} else {
			return null;
		}
	}





	
}
