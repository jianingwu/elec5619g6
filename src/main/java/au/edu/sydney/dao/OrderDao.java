package au.edu.sydney.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Order;


@Repository(value = "orderDao")
public class OrderDao {

	protected final Log logger=LogFactory.getLog(getClass());
	
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void CreateOrder(Order order) {
		System.out.println(order);
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
//		currentSession.clear();
		//sync entity and database record session 
		currentSession.save(order);
		currentSession.flush();
		
	}
	
	public Order getOrder(long orderId) {
		Session currentSession=this.sessionFactory.getCurrentSession();
		Criteria criteria=currentSession.createCriteria(Order.class);
		criteria.add(Restrictions.eq("id", orderId));
		Order order=(Order) criteria.uniqueResult();
		if(order!=null)
		{
			return order;
		} else {
			return null;
		}
		
	}
	public List<Order> showOrders(String keyword){
		if(keyword=="")
		{
			Session currentSession=sessionFactory.openSession();
			Transaction tx=currentSession.beginTransaction();
			String sql="from Order";
			Query order=currentSession.createQuery(sql);
			List<Order> orderList=(List<Order>) order.list();
			return orderList;
		}
		else
		{
			return null;
		}
	}

}
