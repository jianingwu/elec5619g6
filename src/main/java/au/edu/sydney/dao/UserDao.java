package au.edu.sydney.dao;

import java.beans.Expression;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;

@Repository(value = "userDao")
public class UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveUser(User user) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		// save the user..finally
		// sessionFactory.getCurrentSession().save(user);
		currentSession.save(user);
	}

	public String findByUserName(String username) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria criteria = currentSession.createCriteria(User.class);
		criteria.add(Restrictions.eq("userName", username));
		User user = (User) criteria.uniqueResult();
		if (user != null) {
			System.out.println("User found");
			return username;
		} else {
			return null;
		}

	}

//	 public String findByUserNameAndPassword(String username, String password) {
//	 // get current hibernate session
//	 // Session currentSession = sessionFactory.getCurrentSession(); before using
//	 // this occurs transactional error, instead using open,
//	 // maybe due to getcurrentsession is respond with return
//	 Session currentSession = sessionFactory.openSession();
//	 Transaction tx = currentSession.beginTransaction();
//	 String sql = "SELECT userName, password FROM User where userName=:userName
//	 and password=:password";
//	 Query user = currentSession.createQuery(sql);
//	 user.setString("userName", username);
//	 user.setString("password", password);
//	 @SuppressWarnings("unchecked")
//	 List<User> userList = (List<User>) user.list();
//	 currentSession.getTransaction().commit();
//	 currentSession.close();
//	 if (userList.size() >= 1) {
//	 return username;
//	 } else {
//	 return null;
//	 }
//	
//	 }

	public User findUserbyName(String username) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria criteria = currentSession.createCriteria(User.class);
		criteria.add(Restrictions.eq("userName", username));
		User user = (User) criteria.uniqueResult();
		if (user != null) {
			return user;
		} else {
			return null;
		}
	}

	public User validateUser(Login login) {
		User user = findUserbyName(login.getUserName());
		if (user != null) {
			if (user.getPassword().equals(login.getPassword())) {
				return user;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public void addUserDetail(UserDetail userDetail, String userName) {
		User user = findUserbyName(userName);
		if (user != null) {
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			// Get User from databases
			User tempUser = (User) currentSession.get(User.class, user.getId());
			// add details for the user
			tempUser.add(userDetail);
			currentSession.save(userDetail);
		}
	}

	public List<UserDetail> showUserDetail(String userName) {
		User user = findUserbyName(userName);
		if (user != null) {
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			// Get User from databases
			User tempUser = (User) currentSession.get(User.class, user.getId());
			// get details for the user
			List<UserDetail> foundDetail=tempUser.getUserDetail();
			return foundDetail;
		} else {
			return null;
		}
	}

	public void updateUserDetail(UserDetail userDetail, String userName) {
		User user = findUserbyName(userName);
		if (user != null) {
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			// Get User from databases
			User tempUser = (User) currentSession.get(User.class, user.getId());
			// add details for the user
			tempUser.add(userDetail);
			currentSession.clear();
			currentSession.update(userDetail);
			//sync entity and database record session 
			currentSession.flush();
		}
	}

	public void deleteUserDetail(int userDetailId, String userName) {
		User user = findUserbyName(userName);
		if (user != null) {
			//get the current hibernate session
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			// Get User from databases
			User tempUser = (User) currentSession.get(User.class, user.getId());
			//delete userdetail with primary key
			Query theQuery=currentSession.createQuery("delete from UserDetail where id=:userDetailId");
			theQuery.setParameter("userDetailId", userDetailId);
			
			theQuery.executeUpdate();
			
		}
		
	}

	public List<User> findAllUsers() {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		//return a array not json
//		Criteria crit =  currentSession.createCriteria(User.class);
//		ProjectionList userList=Projections.projectionList();
//		userList.add(Projections.property("userName"));
//		userList.add(Projections.property("email"));
//		crit.setProjection(userList);
//		List<User> users=crit.list();
//		return users;
		
//		List<User> users=(List<User>) currentSession.get(User.class, sessionFactory);
		
//		Criteria crit =  currentSession.createCriteria(User.class);
//		ProjectionList userList=Projections.projectionList();
//		userList.add(Projections.property("userName"));
//		crit.setProjection(userList);
		
		Criteria cr = currentSession.createCriteria(User.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property("id"), "id")
						.add(Projections.property("email"), "email")
						.add(Projections.property("userName"), "userName"))
				.setResultTransformer(Transformers.aliasToBean(User.class));

		List<User> users = cr.list();
		return users;

	}
	
	
}
