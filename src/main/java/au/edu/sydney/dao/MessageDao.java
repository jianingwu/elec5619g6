package au.edu.sydney.dao;

import au.edu.sydney.domain.Message;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "messageDao")
public class MessageDao {
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveMessage(Message message) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(message);
    }

    public List<Message> getMessageByReceiverName(String receiverName) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();
        Criteria criteria = currentSession.createCriteria(Message.class);
        criteria.add(Restrictions.eq("mgReceiverName", receiverName));
        List<Message> messageList = (List<Message>) criteria.list();
        return messageList;
    }

    public Message getMessageById(Integer messageId) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Message.class);
        criteria.add(Restrictions.eq("id", messageId));
        Message message = (Message) criteria.uniqueResult();
        return message;
    }

    public void deleteMessage(Integer messageId) {
        Session session = sessionFactory.getCurrentSession();
        Message m = getMessageById(messageId);
        if (m != null) {
//            session.beginTransaction();
            Query theQuery = session.createQuery("delete from Message where id=:MessageId");
            theQuery.setParameter("MessageId", messageId);
            theQuery.executeUpdate();
//            session.getTransaction().commit();
        }
    }
}
