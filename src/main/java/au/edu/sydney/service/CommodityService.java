package au.edu.sydney.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.CommodityDao;
import au.edu.sydney.domain.Commodity;

@Service(value = "commodityService")
public class CommodityService {
	protected final Log logger=LogFactory.getLog(getClass());
	@Autowired
	private CommodityDao commodityDao;
	
	@Transactional
	public Commodity getCommodity(long commodityId) {
		return commodityDao.getCommodity(commodityId);
	}
	
	@Transactional
	public void saveCommodity(Commodity commodity)
	{
		commodityDao.saveCommodity(commodity);
	}
	
	@Transactional
	public List<Commodity> showCommodities(String keyword)
	{
		return commodityDao.showCommodities(keyword);
	}
	
	@Transactional
	public void addCommodity(Commodity commodity) {
		commodityDao.addCommodity(commodity);
	}
	
	@Transactional
	public void updateCommodity(Commodity commodity, long commodityId){
		//logger.info("this is service"+commodity);
		commodityDao.updateCommodity(commodity, commodityId);
	}
	
	@Transactional
	public void deleteCommodity(long commodityId) {
		commodityDao.deleteCommodity(commodityId);
	}
	
	@Transactional
	public boolean changeNumber(int num, Commodity commodity, long commodityId)
	{
		return commodityDao.changeNumber(num, commodity, commodityId);
	}
}
