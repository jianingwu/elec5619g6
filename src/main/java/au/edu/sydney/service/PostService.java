package au.edu.sydney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.PostDao;
import au.edu.sydney.domain.Post;

@Service(value="postService")
public class PostService {
	@Autowired
	private PostDao postDao;
	
	@Transactional
	public Post getPost(long postId) {
		return postDao.getPost(postId);
		
	}
	
	@Transactional
	public void savePost(Post post)
	{
		postDao.savePost(post);
	}
	@Transactional
	public List<Post> showPost(String keyword)
	{
		return postDao.showPost(keyword);
		
	}
	@Transactional
	public void setUserName(Post post, String first_name, String last_name) {
		postDao.set_user_name(post, first_name, last_name);
	}

}
