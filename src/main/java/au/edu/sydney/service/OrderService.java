package au.edu.sydney.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.domain.Order;

@Service(value = "orderService")
public class OrderService {

	protected final Log logger=LogFactory.getLog(getClass());
	@Autowired
	private OrderDao orderDao;
	
	@Transactional
	public Order getOrder(long orderId) {
		return orderDao.getOrder(orderId);
	}
	
	@Transactional
	public void createOrder(Order order) {
		orderDao.CreateOrder(order);
	}
	
	@Transactional
	public List<Order> showOrders(String keyword){
		return orderDao.showOrders(keyword);
	}
}
