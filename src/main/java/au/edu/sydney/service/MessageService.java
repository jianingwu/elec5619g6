package au.edu.sydney.service;

import au.edu.sydney.dao.MessageDao;
import au.edu.sydney.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(value = "messageService")
public class MessageService {
    @Autowired
    private MessageDao messageDao;

    public MessageDao getMessageDao() {
        return messageDao;
    }

    public void setMessageDao(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Transactional
    public void saveMessage(Message message) {
        messageDao.saveMessage(message);
    }


    @Transactional
    public List<Message> getMessagesByRecieverName(String receiverName) {
        return messageDao.getMessageByReceiverName(receiverName);
    }

    @Transactional
    public void deleteMessageById(Integer messageId){
        messageDao.deleteMessage(messageId);
    }

    @Transactional
    public void getMessageById(Integer messageId){
        messageDao.deleteMessage(messageId);
    }
}
