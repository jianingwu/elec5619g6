package au.edu.sydney.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.UserDetailDao;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;


@Service(value="userDetailService")
public class UserDetailService {
	

	@Autowired
	private UserDetailDao userDetailDao;
	
	

	// business logic of registering a User into the database
	@Transactional
	public void saveUserDetail(UserDetail userDetail) {
		// Step 2: if not, save this user into the database
		// User u=findByUserName(user.getUserName());
		userDetailDao.saveUserDetail(userDetail);

	}


	@Transactional
	public UserDetail getUserDetail(int userDetailId) {
		return userDetailDao.getUserDetail(userDetailId);
	}




}
