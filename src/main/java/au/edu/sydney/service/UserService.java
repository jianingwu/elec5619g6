package au.edu.sydney.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.UserDao;
import au.edu.sydney.domain.Login;
import au.edu.sydney.domain.User;
import au.edu.sydney.domain.UserDetail;


@Service(value = "userService")
public class UserService {

	@Autowired
	private UserDao userDao;
	
	

	// business logic of registering a User into the database
	@Transactional
	public void saveUser(User user) {
		// Step 2: if not, save this user into the database
		// User u=findByUserName(user.getUserName());
		userDao.saveUser(user);

	}

	@Transactional
	public String findByUserName(String userName) {
		return userDao.findByUserName(userName);
	}
	
	@Transactional 
	public User findUserByName(String userName) {
		return userDao.findUserbyName(userName);
	}
//	@Transactional
//	public String findByUserNameAndPassword(String username, String password) {
//		return userDao.findByUserNameAndPassword(username,password);
//	}
	
	@Transactional
	public User validateUser(Login login) {
		return userDao.validateUser(login);
	}
	
	@Transactional
	public void addUserDetail(UserDetail userDetail,String userName) {
		userDao.addUserDetail(userDetail,userName);
		
	}
	
	@Transactional
	public List<UserDetail> showUserDetail(String userName) {	
		return userDao.showUserDetail(userName);	
	}

	@Transactional
	public void updateUserDetail(UserDetail userDetail, String userName) {
		userDao.updateUserDetail(userDetail, userName);
		
	}
	
	@Transactional
	public void deleteUserDetail(int userDetailId, String userName) {
		userDao.deleteUserDetail(userDetailId,userName);
		
	}
	
	@Transactional
	public List<User> findAllUsers() {
		return userDao.findAllUsers();
	}
}