package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="comment")
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long comment_id;
	
	@Column(name="user_id")
	private long user_id;
	
	@Column(name="commodity_id")
	private long commodity_id;
	
	@Column(name="comment_time")
	private String comment_time;
	
	@Column(name="comment_content")
	private String comment_content;
	
	@Column(name="if_deleted")
	private boolean if_deleted;
	
	public Comment(long user_id, long commodity_id,String time, String content) {
		this.user_id=user_id;
		this.commodity_id=commodity_id;
		this.comment_time=time;
		this.comment_content=content;
		this.if_deleted=false;
	}

	public long getComment_id() {
		return comment_id;
	}

	public void setComment_id(long comment_id) {
		this.comment_id = comment_id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getCommodity_id() {
		return commodity_id;
	}

	public void setCommodity_id(long commodity_id) {
		this.commodity_id = commodity_id;
	}

	public String getComment_time() {
		return comment_time;
	}

	public void setComment_time(String comment_time) {
		this.comment_time = comment_time;
	}

	public String getComment_content() {
		return comment_content;
	}

	public void setComment_content(String comment_content) {
		this.comment_content = comment_content;
	}

	public boolean isIf_deleted() {
		return if_deleted;
	}

	public void setIf_deleted(boolean if_deleted) {
		this.if_deleted = if_deleted;
	}
	public String toString() {
		return "comment info [ comment id="+comment_id+", user id="+user_id+", commodity_id="+commodity_id+
				", comment time="+comment_time+", comment content="+comment_content+"]";
	}
}
