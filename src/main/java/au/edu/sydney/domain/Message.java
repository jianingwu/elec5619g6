package au.edu.sydney.domain;

import javax.persistence.*;

@Entity
@Table(name = "messageTable")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    private int mgSenderId;
    private int mgReceiverId;

    @Column(name = "mgTitle")
    private String mgTitle;

    @Column(name = "mgContent")
    private String mgContent;

    @Column(name = "mgSenderName")
    private String mgSenderName;

    @Column(name = "mgReceiverName")
    private String mgReceiverName;

    public Message() {
    }

    public Message(int mgSenderId, int mgReceiverId, String mgTitle, String mgContent, String mgSenderName, String mgReceiverName) {
        this.mgSenderId = mgSenderId;
        this.mgReceiverId = mgReceiverId;
        this.mgTitle = mgTitle;
        this.mgContent = mgContent;
        this.mgSenderName = mgSenderName;
        this.mgReceiverName = mgReceiverName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMgSenderId() {
        return mgSenderId;
    }

    public void setMgSenderId(int mgSenderId) {
        this.mgSenderId = mgSenderId;
    }

    public int getMgReceiverId() {
        return mgReceiverId;
    }

    public void setMgReceiverId(int mgReceiverId) {
        this.mgReceiverId = mgReceiverId;
    }

    public String getMgTitle() {
        return mgTitle;
    }

    public void setMgTitle(String mgTitle) {
        this.mgTitle = mgTitle;
    }

    public String getMgContent() {
        return mgContent;
    }

    public void setMgContent(String mgContent) {
        this.mgContent = mgContent;
    }

    public String getMgSenderName() {
        return mgSenderName;
    }

    public void setMgSenderName(String mgSenderName) {
        this.mgSenderName = mgSenderName;
    }

    public String getMgReceiverName() {
        return mgReceiverName;
    }

    public void setMgReceiverName(String mgReceiverName) {
        this.mgReceiverName = mgReceiverName;
    }
}
