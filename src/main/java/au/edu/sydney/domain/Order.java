package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="snap_order")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="user_id")
	private long user_id;
	
	@Column(name="commodity_id")
	private long commodity_id;
	
	@Column(name="com_amount")
	private long com_amount;
	
	@Column(name="com_name")
	private String com_name;
	
	@Column(name="price")
	private float price;
	
	@Column(name="total_price")
	private float total_price;
	
	@Column(name="first_name")
	private String first_name;
	
	@Column(name="last_name")
	private String last_name;
	
	@Column(name="phone_number")
	private String phone_number;
	
	@Column(name="address")
	private String address;
	
	@Column(name="postcode")
	private int postcode;
	
	@Column(name="order_time")
	private String order_time;
	
	@Column(name="if_deleted")
	private boolean if_deleted;
	

	
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Order() {
		super();
	}
	public Order(long user_id, long commodity_id, long amount, String com_name, float price, String first_name,
			String last_name, String phone_number, String address, int postcode) {
		super();
		this.user_id=user_id;
		this.commodity_id=commodity_id;
		this.com_amount=amount;
		this.com_name=com_name;
		this.price=price;
		this.total_price=price*amount;
		this.first_name=first_name;
		this.last_name=last_name;
		this.phone_number=phone_number;
		this.address=address;
		this.postcode=postcode;
		this.order_time="";
		this.if_deleted=false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getCommodity_id() {
		return commodity_id;
	}

	public void setCommodity_id(long commodity_id) {
		this.commodity_id = commodity_id;
	}

	public long getCom_amount() {
		return com_amount;
	}

	public void setCom_amount(long com_amount) {
		this.com_amount = com_amount;
	}

	public String getCom_name() {
		return com_name;
	}

	public void setCom_name(String com_name) {
		this.com_name = com_name;
	}

	public float getTotal_price() {
		return total_price;
	}

	public void setTotal_price(float total_price) {
		this.total_price = total_price;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public String getOrder_time() {
		return order_time;
	}

	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}

	public boolean isIf_deleted() {
		return if_deleted;
	}

	public void setIf_deleted(boolean if_deleted) {
		this.if_deleted = if_deleted;
	}
	
	public String toString() {
		return "This is order:["+user_id+","+
		commodity_id+","+
		com_amount+","+
		com_name+","+
		price+","+
		total_price+","+
		first_name+","+
		last_name+","+
		phone_number+","+
		address+","+
		postcode+","+
		order_time+","+
		if_deleted+"]";
	}
}
