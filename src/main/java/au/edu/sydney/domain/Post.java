package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="post")
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;

	@Column(name="post_title")
	private String post_title;
	
	
	@Column(name="user_id")
	private Long user_id;
	
	@Column(name="post_user_name")
	private String post_user_name;
	
	@Column(name="post_pic1")
	private String post_pic1;
	
	@Column(name="post_pic2")
	private String post_pic2;
	
	@Column(name="post_pic3")
	private String post_pic3;
	
	@Column(name="post_content")
	private String post_content;
	
	@Column(name="post_like_amount")
	private Long post_like_amount;
	
	@Column(name="post_time")
	private String post_time;
	
	@Column(name="if_deleted")
	private boolean if_deleted;
	
	public Post() {
		super();
	}
	
	public Post(String title, Long user_id,String user_name, String pic1, 
			String pic2, String pic3, String content ) {
		super();
		this.post_title=title;
		this.user_id=user_id;
		this.post_user_name=user_name;
		this.post_time="";
		this.post_pic1=pic1;
		this.post_pic2=pic2;
		this.post_pic3=pic3;
		this.post_like_amount=(long) 0;
		this.post_content=content;
		this.if_deleted=false;
		
	}
	
	
	
	public String getPost_title() {
		return post_title;
	}

	public void setPost_title(String post_title) {
		this.post_title = post_title;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	
	

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getPost_user_name() {
		return post_user_name;
	}



	public void setPost_user_name(String post_user_name) {
		this.post_user_name = post_user_name;
	}



	public String getPost_pic1() {
		return post_pic1;
	}

	public void setPost_pic1(String post_pic1) {
		this.post_pic1 = post_pic1;
	}

	public String getPost_pic2() {
		return post_pic2;
	}

	public void setPost_pic2(String post_pic2) {
		this.post_pic2 = post_pic2;
	}

	public String getPost_pic3() {
		return post_pic3;
	}

	public void setPost_pic3(String post_pic3) {
		this.post_pic3 = post_pic3;
	}

	public String getPost_content() {
		return post_content;
	}

	public void setPost_content(String post_content) {
		this.post_content = post_content;
	}

	public Long getPost_like_amount() {
		return post_like_amount;
	}

	public void setPost_like_amount(Long post_like_amount) {
		this.post_like_amount = post_like_amount;
	}

	public String getPost_time() {
		return post_time;
	}

	public void setPost_time(String post_time) {
		this.post_time = post_time;
	}

	public boolean isIf_deleted() {
		return if_deleted;
	}

	public void setIf_deleted(boolean if_deleted) {
		this.if_deleted = if_deleted;
	}

}
