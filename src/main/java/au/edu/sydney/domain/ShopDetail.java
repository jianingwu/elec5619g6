package au.edu.sydney.domain;

public class ShopDetail {

	private long commodity_id;
	private long user_id;
	public long getCommodity_id() {
		return commodity_id;
	}
	public void setCommodity_id(long commodity_id) {
		this.commodity_id = commodity_id;
	}
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	
	public ShopDetail(long commodity_id, long user_id){
		this.commodity_id=commodity_id;
		this.user_id=user_id;
	}
	public ShopDetail() {
		
	}
	public String toString() {
		return "shopDetail [ commodity id ="+commodity_id+", user_id="+user_id+"]";
	}
}
