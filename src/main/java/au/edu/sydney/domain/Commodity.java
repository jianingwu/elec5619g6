package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="commodity")
public class Commodity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;

	@Column(name="com_name")
	private String com_name;

	@Column(name="com_amount")
	private long com_amount;
	
	@Column(name="com_price")
	private float com_price;
	
	@Column(name="com_discount")
	private float com_discount;

	@Column(name="real_price")
	private float real_price;
	
	@Column(name="com_type")
	private String com_type;

	@Column(name="com_describe")
	private String com_describe; 

	@Column(name="com_pic")
	private String com_pic;

	@Column(name="com_size")
	private String com_size;

	@Column(name="com_saled_amount")
	private long com_saled_amount;

	@Column(name="com_color")
	private String com_color;

	@Column(name="com_rank_total")
	private float com_rank_total;

	@Column(name="com_rank_num")
	private long com_rank_num;

	@Column(name="if_deleted")
	private boolean if_deleted;
	public Commodity()
	{
		super();
	}
	
	
	public float getReal_price() {
		return real_price;
	}
	public void setReal_price(float real_price) {
		this.real_price = real_price;
	}
	

	public Commodity(String name, long amount, float price,float discount,String type,String size
			,String describe,String pic, String color) {
		super();
		this.com_name=name;
		this.com_amount=amount;
		this.com_price=price;
		this.com_discount=discount;
		this.com_type=type;
		this.com_describe=describe;
		this.com_pic=pic;
		this.com_size=size;
		this.com_saled_amount=0;
		this.com_color=color;
		this.com_rank_total=0;
		this.com_rank_num=0;
		this.real_price=price*discount;
		this.if_deleted=false;
	}
	public void refreshCommodity(Commodity commodity,Long id) {
		this.id=id;
		this.com_name=commodity.getCom_name();
		this.com_amount=commodity.getCom_amount();
		this.com_price=commodity.getCom_price();
		this.com_discount=commodity.getCom_discount();
		this.com_type=commodity.getCom_type();
		this.com_describe=commodity.getCom_describe();
		this.com_pic=commodity.getCom_pic();
		this.com_size=commodity.getCom_size();
		this.com_saled_amount=commodity.getCom_saled_amount();
		this.com_color=commodity.getCom_color();
		this.com_rank_total=commodity.getCom_rank_total();
		this.com_rank_num=commodity.getCom_rank_num();
		this.real_price=commodity.getCom_discount()*commodity.getCom_price();
		this.if_deleted=commodity.getIf_deleted();
	}
	
	public long getId() {
		return id;
	}
	
	
	public void setId(long id) {
		this.id = id;
	}

	public void setIf_deleted(boolean if_deleted) {
		this.if_deleted = if_deleted;
	}

	public boolean getIf_deleted() {
		return if_deleted;
	}
	
	public String getCom_name() {
		return com_name;
	}


	public void setCom_name(String com_name) {
		this.com_name = com_name;
	}


	public long getCom_amount() {
		return com_amount;
	}


	public void setCom_amount(long com_amount) {
		this.com_amount = com_amount;
	}


	public float getCom_price() {
		return com_price;
	}


	public void setCom_price(float com_price) {
		this.com_price = com_price;
	}


	public float getCom_discount() {
		return com_discount;
	}


	public void setCom_discount(float com_discount) {
		this.com_discount = com_discount;
	}


	public String getCom_type() {
		return com_type;
	}


	public void setCom_type(String com_type) {
		this.com_type = com_type;
	}


	public String getCom_describe() {
		return com_describe;
	}


	public void setCom_describe(String com_describe) {
		this.com_describe = com_describe;
	}


	public String getCom_pic() {
		return com_pic;
	}


	public void setCom_pic(String com_pic) {
		this.com_pic = com_pic;
	}


	public String getCom_size() {
		return com_size;
	}


	public void setCom_size(String com_size) {
		this.com_size = com_size;
	}


	public long getCom_saled_amount() {
		return com_saled_amount;
	}


	public void setCom_saled_amount(long com_saled_amount) {
		this.com_saled_amount = com_saled_amount;
	}


	public String getCom_color() {
		return com_color;
	}


	public void setCom_color(String com_color) {
		this.com_color = com_color;
	}


	public float getCom_rank_total() {
		return com_rank_total;
	}


	public void setCom_rank_total(float com_rank_total) {
		this.com_rank_total = com_rank_total;
	}


	public long getCom_rank_num() {
		return com_rank_num;
	}


	public void setCom_rank_num(long com_rank_num) {
		this.com_rank_num = com_rank_num;
	}

	@Override
	public String toString() {
		return "commodity info [ id= "+id+", name="+com_name+", amount="+com_amount+", discount="+
	com_discount+", type="+com_type+", discribe="+com_describe+", pic="+com_pic+", size="+com_size+", saled_amount="+
				com_saled_amount+", color="+com_color+", rank_total="+com_rank_total+", rank_num="+com_rank_num+"]";
	}
}
