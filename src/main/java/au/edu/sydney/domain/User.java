package au.edu.sydney.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//make id is auto increment,also GenerationType.IDENTITY  is also fine
	@Column(name="id")
	private int id;
	//mapping the java fields to the elec5619g6 user database column, 
	//so the name of the column should be the same to the name of the database
	@NotNull(message="is required")
	@Size(min=1,message="is required")
	@Column(name="user_name")
	private String userName;
	
	@NotNull(message="is required")
	@Size(min=1,message="is required")
	@Column(name="email")
	private String email;
	
	@NotNull(message="is required")
	@Size(min=1,message="is required")
	@Column(name="password")
	private String password;
	
	@OneToMany(mappedBy="user" ,fetch=FetchType.EAGER, cascade= {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH,})
	private List<UserDetail> userDetail;
	
	public User() {
		
	}


	public User(String userName, String email, String password) {
		super();
		this.userName = userName;
		this.email = email;
		this.password = password;
	}

	//generate the getter and setters
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getEmail() {
		return email;
	}
	

	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	
	public List<UserDetail> getUserDetail() {
		return userDetail;
	}


	public void setUserDetail(List<UserDetail> userDetail) {
		this.userDetail = userDetail;
	}
	
	//add convenience methods for bi-directional relationship
	public void add(UserDetail tempUserDetail) {
		if (userDetail==null) {
			userDetail=new ArrayList<>();
		}
		
		userDetail.add(tempUserDetail);
		tempUserDetail.setUser(this);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", email=" + email + ", password=" + password + "]";
	}
	
	
	
}
