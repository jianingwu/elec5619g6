<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>commodity manage page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<style>
.maindiv
{
	margin-top:80px;
	margin-right:0px;
	margin-left:0px;
	padding-bottom:100%;
	background-color:#f0f0f0;
}

.leftbar
{
	position:fixed;
	top:80px;
	left:3%;
	width:20%;
	min-width:100px;
	height:50%;
	min-height:100px;
	background-color:#ffffff;
	border:1px solid gray;
	box-shadow: 3px 3px 3px #aaaaaa;
	text-overflow:ellipsis;
}
.rightbar
{
	position:fixed;
	top:80px;
	right:3%;
	width:20%;
	min-width:100px;
/* 	max-height:500px; */
 	min-height:300px; 
	background-color:#ffffff;
	border:1px solid gray;
	box-shadow: 3px 3px 3px #aaaaaa
}

.midarea
{
	margin:auto;
	width:50%;
	min-height:1000px;
	border:1px solid gray;
	background-color:#ffffff;
}
.navigate-bar-main
{
	background-color:#1592e6;
}
.navigate-bar-items
{
	margin-left:20px;
	margin-right:20px;
	width:10%;
	font-family:"Arial";
	font-size:20px;
	color:white;
	text-align:center;
}
.populars
{
	margin-left:10%;
	color:black;
	font-size:30px;
}
.populars2
{
	margin-left:10%;
	color:grey;
	font-size:10px;
}
.rightbartitle
{
	text-align:center;
	color:#fcc52c;
	height:10%;
	font-size:20px;
}
.mainname
{
	height:30%;
	font-size:40px;
	font-weight:4px;
	color:#fcc52c;
}
.commoditytab
{
	display:inline-block;
	margin-top:10px;
	height:300px;
	width:256px;
	margin-left:2.5%;
	border:1px solid gray;
	box-shadow: 1px 1px 1px #aaaaaa;
	
}
</style>
<body>
	<nav
		class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
		<a class="navigate-bar-items" href="showMainPage">SnapShop</a> 
		<a class="navigate-bar-items" href="showPostPage">Posts</a> 
		<a class="navigate-bar-items" href="showMainPage">Shops</a> 
		<a class="navigate-bar-items" href="#">Messages</a>
		<input class="form-control form-control-dark w-100" type="text"
			placeholder="Search" aria-label="Search"> 
		<a class="navigate-bar-items" href="showHomePage">Profile</a>
		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap"><a class="nav-link"
				href="logout">Log Out</a></li>
		</ul>
	</nav>
	<div class="maindiv">
		<div class="leftbar">
			<a class="mainname"><c:out value="${userDetail.firstName}"/>  <c:out value="${userDetail.lastName}"/></a>
			<hr/>
			<h5>Email: <c:out value="${userDetail.email}"/></h5>
			<hr/>
			<h5>phoneNumber: <c:out value="${userDetail.phoneNumber}"/></h5>
			<hr/>
			<h5>city: <c:out value="${userDetail.city}"/></h5>
			<hr/>
			<h5>state: <c:out value="${userDetail.state}"/></h5>
			<hr/>
		</div>
		<div class="midarea">
			<div id="MainShownArea">
				<c:forEach var="commodity" items="${commodities}">
					<div class="commoditytab">
						<img width=256px height=144px >
						<div style="width:60%; height:156px; display:inline-block;">
							<div style="margin: 30px 10px 10px 10px;">
								<h5 ><c:out value="${commodity.com_name}" /></h5>
								<h6 style="color:orange; margin-right:2px"><c:out value="${commodity.real_price}" />AUD</h6>
								<b> rank:  <c:out value="${commodity.com_rank_total}" /></b>
<%-- 								<b> rank number: <c:out value="${commodity.com_rank_num}" /></b> --%>
							</div>
						</div>
						<div style="display:inline-block;">
							<form:form action="checkCommodityDetail" ModelAttribute="shopDetail" 
							method="POST">
								<input type="hidden" name="commodity_id" value="${commodity.id}" >
								<input type="hidden" name="user_id" value="${user.id}" >
								<button type="submit">viewDetail</button>
							</form:form>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="rightbar">
			<div>
				<div class="rightbartitle"> Most Popular Users
				</div>
				<hr/>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
				<script>
					$.ajax({
						url:'/sydney/listAllUsers',
						dataType:'JSON',
						type:'get',
						cache: false,
						success:function(data){
							console.log(data);
							$(data).each(function(index,value){
								console.log(value.userName);
								document.createElement("div");
								var a="<a class=\"populars\">"+value.userName+"</a>";
								var d="<br />"
								var b="<a class=\"populars2\">"+value.email+"</a>";
								var c="<hr />"
								$('#popularArea').append(a);
								$('#popularArea').append(d);
								$('#popularArea').append(b);
								$('#popularArea').append(c);
							});
						},
						error:function(data){
							consloe.log("data loading error");
						}
					})
// 					$.post("/listAllUsers", function(data) {
// 					  console.log('It Worked');
// 					});
				</script>
				<div id="popularArea"></div>
			</div>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
	</script>
<!-- 	<script src="../../assets/js/vendor/popper.min.js"></script> -->
<!-- 	<script src="../../dist/js/bootstrap.min.js"></script> -->

	<!-- Icons -->
	<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
	<script>
		feather.replace()
	</script>




</body>


</html>

