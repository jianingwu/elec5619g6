<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html lang="en">
<head>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<title>Log In</title>

</head>
<body>

<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
	<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">SnapShop</a> 
	<span>
	<a class="btn btn-outline-light" href="showRegisterForm" role="button">Sign Up</a>
	<a class="btn btn-outline-light" href="showLogInForm" role="button">Sign In</a>
	</span>
</nav>

<hr class="mb-4">
<hr class="mb-4">

<div class="row justify-content-md-center">
	<h2 class="mb-3 ">Log In</h2>
</div>

<div class="container">
		<div class="row justify-content-md-center">
				<div class="col-4">
				<form:form action="login" method="POST" modelAttribute="login"
					class="form-signin">
					<h1 class="mb-3">${msg}</h1>
					<div class="mb-3">
						<label for="postcode">User Name</label><span class="text-muted">(Required)</span>
						<input type="text" name="userName" path="userName"
							class="form-control" id="userName" required="">
						<div>
							<form:errors path="userName" cssClass="invalid-feedback" />
						</div>
						<div class="invalid-feedback">Please enter your User Name</div>
					</div>

					<div class="mb-3">
						<label for="password">Password <span class="text-muted">(Required)</span></label>
						<input type="password" name="password" path="password"
							class="form-control" id="password" placeholder="">
						<div class="invalid-feedback">Please enter a valid password</div>
					</div>


					<hr class="mb-3">
					<button class="btn btn-success btn-lg btn-block" type="submit">Sign
						In</button>

				</form:form>
			
			</div>

		</div>
	</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous">
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous">
</script>

<script
src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous">
</script>

</body>
</html>