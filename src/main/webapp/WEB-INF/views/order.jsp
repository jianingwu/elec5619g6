<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>commodity manage page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<style>
.maindiv
{
	margin-top:80px;
	margin-right:0px;
	margin-left:0px;
	padding-bottom:100%;
	background-color:#f0f0f0;
}

.leftbar
{
	position:fixed;
	top:80px;
	left:3%;
	width:20%;
	min-width:100px;
	height:50%;
	min-height:100px;
	background-color:#ffffff;
	border:1px solid gray;
	box-shadow: 3px 3px 3px #aaaaaa;
	text-overflow:ellipsis;
}
.rightbar
{
	position:fixed;
	top:80px;
	right:3%;
	width:20%;
	min-width:100px;
/* 	max-height:500px; */
 	min-height:300px; 
	background-color:#ffffff;
	border:1px solid gray;
	box-shadow: 3px 3px 3px #aaaaaa
}

.midarea
{
	margin:auto;
	width:50%;
	min-height:1000px;
	border:1px solid gray;
	background-color:#ffffff;
}
.navigate-bar-main
{
	background-color:#1592e6;
}
.navigate-bar-items
{
	margin-left:20px;
	margin-right:20px;
	width:10%;
	font-family:"Arial";
	font-size:20px;
	color:white;
	text-align:center;
}
.populars
{
	margin-left:10%;
	color:black;
	font-size:30px;
}
.populars2
{
	margin-left:10%;
	color:grey;
	font-size:10px;
}
.rightbartitle
{
	text-align:center;
	color:#fcc52c;
	height:10%;
	font-size:20px;
}
.mainname
{
	height:30%;
	font-size:40px;
	font-weight:4px;
	color:#fcc52c;
}
.commoditytab
{
	display:inline-block;
	margin-top:10px;
	height:300px;
	width:256px;
	margin-left:2.5%;
	border:1px solid gray;
	box-shadow: 1px 1px 1px #aaaaaa;
	
}
.detialinfo
{
	font-size:20px;
}
</style>
<body>
	<nav
		class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
		<a class="navigate-bar-items" href="showMainPage">SnapShop</a> 
		<a class="navigate-bar-items" href="showPostPage">Posts</a> 
		<a class="navigate-bar-items" href="showMainPage">Shops</a> 
		<a class="navigate-bar-items" href="#">Messages</a>
		<input class="form-control form-control-dark w-100" type="text"
			placeholder="Search" aria-label="Search"> 
		<a class="navigate-bar-items" href="showHomePage">Profile</a>
		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap"><a class="nav-link"
				href="logout">Log Out</a></li>
		</ul>
	</nav>
	<div class="maindiv">
		<div class="leftbar">
			<a class="mainname"><c:out value="${userDetail.firstName}"/>  <c:out value="${userDetail.lastName}"/></a>
			<hr/>
			<h5>Email: <c:out value="${userDetail.email}"/></h5>
			<hr/>
			<h5>phoneNumber: <c:out value="${userDetail.phoneNumber}"/></h5>
			<hr/>
			<h5>city: <c:out value="${userDetail.city}"/></h5>
			<hr/>
			<h5>state: <c:out value="${userDetail.state}"/></h5>
			<hr/>
		</div>
		<div class="midarea">
			<div id="MainShownArea">
				<div id="detailarea">
					<div id="userinfoarea" style="margin-top:5%;margin-left:5%; margin-right:5%;">
						<form:form action="addOrder" ModelAttribute="order"
						method="POST" class="needs-validation" novalidate="">
						<input type="hidden" name="user_id" value="${user.id}" >
						<input type="hidden" name="commodity_id" value="${commodity.id}" >
						
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="com_amount">buy amount</label> <input type="text"
									name="com_amount" class="form-control" id="com_amount"
									placeholder="" value="1 " required="">
								<div class="invalid-feedback">Valid Commodity amount is
									required.</div>
							</div>
						</div>
						<input type="hidden" name="com_name" value="${commodity.com_name}" >
						<input type="hidden" name="price" value="${commodity.real_price}" >
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="first_name">first name</label> <input type="text"
									name="first_name" class="form-control" id="first_name"
									placeholder="" value="${userDetail.firstName}" required="">
								<div class="invalid-feedback">Valid first name is
									required.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="last_name">last name</label> <input type="text"
									name="last_name" class="form-control" id="last_name"
									placeholder="" value="${userDetail.lastName}" required="">
								<div class="invalid-feedback">Valid last name is required.
								</div>
							</div>
						</div>

						<div class="mb-3">
							<label for="phone_number">phone number <span class="text-muted">(Required)</span></label>
							<input type="text" name="phone_number" class="form-control" id="phone_number"  value="${userDetail.phoneNumber}">
							<div class="invalid-feedback">Valid phone number is required.</div>
						</div>

						<div class="mb-3">
							<label for="address">address <span
								class="text-muted">(Required)</span></label> <input type="text"
								name="address" class="form-control" id="address" value="${userDetail.street}">
							<div class="invalid-feedback">Valid address is required.</div>
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="postcode">post code</label> <input type="text"
									name="postcode" class="form-control" id="postcode"
									placeholder="" value="${userDetail.postcode}" required="">
								<div class="invalid-feedback">Valid post code is
									required.</div>
							</div>
						</div>

						
						<hr class="mb-4">
						<button class="btn btn-primary btn-lg btn-block" type="submit" onclick="if(!(confirm('Are you going to add this order?'))) return false">Submit</button>

					</form:form>
					</div>
					<div id="bankinfoarea">
					</div>
				</div>
			</div>
		</div>
		<div class="rightbar">
			<div>
				<img width=256px height=144px >
					<div style="width:60%; height:156px; display:inline-block;">
						<div style="margin: 30px 10px 10px 10px;">
							<h5 ><c:out value="${commodity.com_name}" /></h5>
							<h6 style="color:orange; margin-right:2px"><c:out value="${commodity.real_price}" />AUD</h6>
							<b> rank:  <c:out value="${commodity.com_rank_total}" /></b>
<%-- 							<b> rank number: <c:out value="${commodity.com_rank_num}" /></b> --%>
						</div>
					</div>
			</div>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
	</script>
<!-- 	<script src="../../assets/js/vendor/popper.min.js"></script> -->
<!-- 	<script src="../../dist/js/bootstrap.min.js"></script> -->

	<!-- Icons -->
	<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
	<script>
		feather.replace()
	</script>




</body>


</html>

