<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


<title>Loading Page</title>

</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
	<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">SnapShop</a> 
	<span>
	<a class="btn btn-outline-light" href="showRegisterForm" role="button">Sign Up</a>
	<a class="btn btn-outline-light" href="showLogInForm" role="button">Sign In</a>
	</span>
</nav>

<hr class="mb-4">
<hr class="mb-4">
<div class="row justify-content-md-center">
	<h2 class="mb-3 ">Welcome to Snapshop</h2>
</div>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-6 col-md-4"><a class="btn btn-outline-primary btn-block" href="showRegisterForm" role="button">Sign Up</a></div>
		<div class="col-6 col-md-4"><a class="btn btn-outline-success btn-block" href="showLogInForm" role="button">Sign In</a></div>
	</div>	
</div>

	
	
</body>
</html>