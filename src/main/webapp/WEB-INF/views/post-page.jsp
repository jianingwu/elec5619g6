<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>commodity manage page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<style>
.maindiv
{
	margin-top:80px;
	margin-right:0px;
	margin-left:0px;
	padding-bottom:100%;
	background-color:#f0f0f0;
}

.leftbar
{
	position:fixed;
	top:80px;
	left:3%;
	width:20%;
	min-width:100px;
	height:50%;
	min-height:100px;
	background-color:#ffffff;
	border:1px solid gray;
	box-shadow: 3px 3px 3px #aaaaaa;
	text-overflow:ellipsis;
}
.rightbar
{
	position:fixed;
	top:80px;
	right:3%;
	width:20%;
	min-width:100px;
/* 	max-height:500px; */
 	min-height:300px; 
	background-color:#ffffff;
	border:1px solid gray;
	box-shadow: 3px 3px 3px #aaaaaa
}

.midarea
{
	margin:auto;
	width:50%;
	min-width:700px;
	min-height:1000px;
	border:1px solid gray;
	background-color:#ffffff;
}
.navigate-bar-main
{
	background-color:#1592e6;
}
.navigate-bar-items
{
	margin-left:20px;
	margin-right:20px;
	width:10%;
	font-family:"Arial";
	font-size:20px;
	color:white;
	text-align:center;
}
.populars
{
	margin-left:10%;
	color:black;
	font-size:30px;
}
.populars2
{
	margin-left:10%;
	color:grey;
	font-size:10px;
}
.rightbartitle
{
	text-align:center;
	color:#fcc52c;
	height:10%;
	font-size:20px;
}
.mainname
{
	height:30%;
	font-size:40px;
	font-weight:4px;
	color:#fcc52c;
}
.commoditytab
{
	display:inline-block;
	margin-top:10px;
	height:300px;
	width:256px;
	margin-left:2.5%;
	border:1px solid gray;
	box-shadow: 1px 1px 1px #aaaaaa;
	
}
.posttab
{
	margin-left:10%;
	margin-right:10%;
	mid-width:400px;
}
</style>
<body>
	<nav
		class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
		<a class="navigate-bar-items" href="showMainPage">SnapShop</a> 
		<a class="navigate-bar-items" href="showPostPage">Posts</a> 
		<a class="navigate-bar-items" href="showMainPage">Shops</a> 
		<a class="navigate-bar-items" href="#">Messages</a>
		<input class="form-control form-control-dark w-100" type="text"
			placeholder="Search" aria-label="Search"> 
		<a class="navigate-bar-items" href="showHomePage">Profile</a>
		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap"><a class="nav-link"
				href="logout">Log Out</a></li>
		</ul>
	</nav>
	<div class="maindiv">
		<div class="leftbar">
			<a class="mainname"><c:out value="${userDetail.firstName}"/>  <c:out value="${userDetail.lastName}"/></a>
			<hr/>
			<h5>Email: <c:out value="${userDetail.email}"/></h5>
			<hr/>
			<h5>phoneNumber: <c:out value="${userDetail.phoneNumber}"/></h5>
			<hr/>
			<h5>city: <c:out value="${userDetail.city}"/></h5>
			<hr/>
			<h5>state: <c:out value="${userDetail.state}"/></h5>
			<hr/>
		</div>
		<div class="midarea">
			<div id="MainShownArea">
				<div id="CreatePost" style="margin-left:5%; margin-top:5%; margin-right:5%;">
					<form:form action="addPost" ModelAttribute="post"
						method="POST" class="needs-validation" novalidate="">
						
						<div class="mb-3">
							<label for="post_title">Title <span
								class="text-muted">(Required)</span></label> <input type="text"
								name="post_title" class="form-control" id="post_title" value="">
							<div class="invalid-feedback">Valid address is required.</div>
						</div>
						
						<input type="hidden" name="user_id" value="${user.id}" >
						
						<input type="hidden" name="post_user_name" value="${name}" >
						
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="post_pic1">picture 1</label> <input type="text"
									name="post_pic1" class="form-control" id="post_pic1"
									placeholder="" value="" required="">
								<div class="invalid-feedback">Valid picture 1 is
									required.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="post_pic2">picture 2</label> <input type="text"
									name="post_pic2" class="form-control" id="post_pic2"
									placeholder="" value="" required="">
								<div class="invalid-feedback">Valid picture 2 is
									required.</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="post_pic3">picture 3</label> <input type="text"
									name="post_pic3" class="form-control" id="post_pic3"
									placeholder="" value="" required="">
								<div class="invalid-feedback">Valid picture 3 is
									required.</div>
							</div>
						</div>

						<div class="mb-3">
							<label for="post_content">content <span
								class="text-muted">(Required)</span></label> <input type="text" 
								name="post_content" class="form-control" id="post_content">
							<div class="invalid-feedback">Valid content is required.</div>
						</div>

						
						<hr class="mb-4">
						<button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>

					</form:form>
				</div>
				<c:forEach var="post" items="${posts}">
					<div class="posttab">
						<hr />
						<h3><c:out value="${post.post_user_name}" />:<c:out value="${post.post_title}" /></h3>
						<br/>
						<b>publish time: <c:out value="${post.post_time}" /></b>
						<br/>
						<textarea rows="6" cols="80"><c:out value="${post.post_content}" /></textarea>
						<br/>
						<img width=160px height=90px > <img width=160px height=90px > <img width=160px height=90px >
					</div>
				</c:forEach>
				<hr/>
			</div>
		</div>
		<div class="rightbar">
			<div>
				<div class="rightbartitle"> Most Popular Users
				</div>
				<hr/>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
				<script>
					$.ajax({
						url:'/sydney/listAllUsers',
						dataType:'JSON',
						type:'get',
						cache: false,
						success:function(data){
							console.log(data);
							$(data).each(function(index,value){
								console.log(value.userName);
								document.createElement("div");
								var a="<a class=\"populars\">"+value.userName+"</a>";
								var d="<br />"
								var b="<a class=\"populars2\">"+value.email+"</a>";
								var c="<hr />"
								$('#popularArea').append(a);
								$('#popularArea').append(d);
								$('#popularArea').append(b);
								$('#popularArea').append(c);
							});
						},
						error:function(data){
							consloe.log("data loading error");
						}
					})
// 					$.post("/listAllUsers", function(data) {
// 					  console.log('It Worked');
// 					});
				</script>
				<div id="popularArea"></div>
			</div>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
	</script>
<!-- 	<script src="../../assets/js/vendor/popper.min.js"></script> -->
<!-- 	<script src="../../dist/js/bootstrap.min.js"></script> -->

	<!-- Icons -->
	<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
	<script>
		feather.replace()
	</script>




</body>


</html>

