<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>commodity manage page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link href="style.css" rel="stylesheet">
</head>
<body>
	<nav
		class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
		<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">SnapShop Manage</a> 
		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap"><a class="nav-link"
				href="logout">Log Out</a></li>
		</ul>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<a class="nav-link active" href="#"> <svg
								xmlns="http://www.w3.org/2000/svg" width="24" height="24"
								viewBox="0 0 24 24" fill="none" stroke="currentColor"
								stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
								class="feather feather-home">
								</svg> AllCommodities 
								<span class="sr-only">(current)</span>
						</a>
						<li class="nav-item"><a class="nav-link active" href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-home">
									</svg> commodities manage
								<span class="sr-only">(current)</span>
						</a></li>
						<li class="nav-item"><a class="nav-link" href="#"> <svg
									xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-file">
									</svg> users manage
						</a></li>
						<li class="nav-item"><a class="nav-link" href="#"> <svg
									xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-shopping-cart">
									</svg>
								mails manage
						</a></li>
					</ul>				
				</div>
			</nav>

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
					
			</div>
			<div
				class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<span class="h1">${msg}</span>
				<div class="btn-toolbar mb-2 mb-md-0">
					<div class="btn-group mr-2">

						<!--  <input type="button" value="Add New Address" class="btn btn-sm btn-outline-secondary" onclick="window.location.href='showUserDetail';return false;">-->
					</div>
				</div>
			</div>
			<h2></h2>
			<div class="col-md-8 order-md-1">
				<h4 class="mb-3">Add New Commodity</h4>
				<div class="table-responsive">
					<form:form action="updateCommodity" ModelAttribute="commodity"
						method="POST" class="needs-validation" novalidate="">
						<input type="hidden" name="id" value="${id}" >
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="com_name">Commodity Name</label> <input type="text"
									name="com_name" class="form-control" id="com_name"
									placeholder="" value="${com_name}" required="">
								<div class="invalid-feedback">Valid Commodity Name is
									required.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="com_amount">amount</label> <input type="text"
									name="com_amount" class="form-control" id="com_amount"
									placeholder="" value="${com_amount}" required="">
								<div class="invalid-feedback">Valid amount is required.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="com_price">price</label> <input type="text"
									name="com_price" class="form-control" id="com_price"
									placeholder="" value="${com_price}" required="">
								<div class="invalid-feedback">Valid price is
									required.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="com_discount">discount</label> <input type="text"
									name="com_discount" class="form-control" id="com_discount"
									placeholder="" value="${com_discount}" required="">
								<div class="invalid-feedback">Valid discount is required.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="com_type">type</label> <input type="text"
									name="com_type" class="form-control" id="com_type"
									placeholder="" value="${com_type}" required="">
								<div class="invalid-feedback">Valid type is
									required.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="com_size">size</label> <input type="text"
									name="com_size" class="form-control" id="com_size"
									placeholder="" value="${com_size}" required="">
								<div class="invalid-feedback">Valid size is required.
								</div>
							</div>
						</div>

						<div class="mb-3">
							<label for="com_describe">describe <span class="text-muted">(Required)</span></label>
							<input type="text" name="com_describe" class="form-control" id="com_describe" value="${com_describe}">
							<div class="invalid-feedback">Valid describe is required.</div>
						</div>

						<div class="mb-3">
							<label for="com_pic">pic <span
								class="text-muted">(Required)</span></label> <input type="text"
								name="com_pic" class="form-control" id="com_pic" value="${com_pic}">
							<div class="invalid-feedback">Valid pic is required.</div>
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="com_color">color</label> <input type="text"
									name="com_color" class="form-control" id="com_color"
									placeholder="" value="${com_color}" required="">
								<div class="invalid-feedback">Valid color is
									required.</div>
							</div>
						</div>

						
						<hr class="mb-4">
						<button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>

					</form:form>

				</div>


			</div>
			
			
			</main>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
	</script>
	<script src="../../assets/js/vendor/popper.min.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>

	<!-- Icons -->
	<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
	<script>
		feather.replace()
	</script>




</body>


</html>

