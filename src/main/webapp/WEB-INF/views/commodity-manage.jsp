<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>commodity manage page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link href="style.css" rel="stylesheet">
</head>
<body>
	<nav
		class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
		<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">SnapShop Manage</a> 
		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap"><a class="nav-link"
				href="logout">Log Out</a></li>
		</ul>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<a class="nav-link active" href="#"> <svg
								xmlns="http://www.w3.org/2000/svg" width="24" height="24"
								viewBox="0 0 24 24" fill="none" stroke="currentColor"
								stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
								class="feather feather-home">
								</svg> AllCommodities 
								<span class="sr-only">(current)</span>
						</a>
						<li class="nav-item"><a class="nav-link active" href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-home">
									</svg> commodities manage
								<span class="sr-only">(current)</span>
						</a></li>
						<li class="nav-item"><a class="nav-link" href="#"> <svg
									xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-file">
									</svg> users manage
						</a></li>
						<li class="nav-item"><a class="nav-link" href="#"> <svg
									xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-shopping-cart">
									</svg>
								mails manage
						</a></li>
					</ul>				
				</div>
			</nav>

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
					
			</div>
			<div
				class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<span class="h1">${msg}</span>
				<div class="btn-toolbar mb-2 mb-md-0">
					<div class="btn-group mr-2">
						<button class="btn btn-sm btn-outline-secondary"
							value="Add New commodity"
							onclick="window.location.href='showAddCommodity';return false;">Add
							new commodity</button>
						<!--  <input type="button" value="Add New Address" class="btn btn-sm btn-outline-secondary" onclick="window.location.href='showUserDetail';return false;">-->
					</div>
				</div>
			</div>
			<h4 class="mb-3">Commodities information table</h4>
			<div class="table-responsive">
				<table class="table table-striped table-sm">

					<tr>
						<th>name</th>
						<th>amount</th>
						<th>price</th>
						<th>discount</th>
						<th>realprice</th>
						<th>type</th>
						<th>discribe</th>
						<th>pic</th>
						<th>size</th>
						<th>saled-amount</th>
						<th>color</th>
						<th>rank-total</th>
						<th>rank-num</th>
						<th>if-deleted</th>
						<th>Action</th>
					</tr>
					<c:forEach var="commodity" items="${commodities}">

						put an update link with user id
						<c:url var="updateLink" value="/showCommodityFormUpdate">
							<c:param name="commodityId" value="${commodity.id}" />
						</c:url>

						put an delete link with user id
						<c:url var="deleteLink" value="/deleteCommodity">
							<c:param name="commodityId" value="${commodity.id}" />
						</c:url>
						<tr>
							<td><c:out value="${commodity.com_name}" /></td>
							<td><c:out value="${commodity.com_amount}" /></td>
							<td><c:out value="${commodity.com_price}" /></td>
							<td><c:out value="${commodity.com_discount}" /></td>
							<td><c:out value="${commodity.real_price}" /></td>
							<td><c:out value="${commodity.com_type}" /></td>
							<td><c:out value="${commodity.com_describe}" /></td>
							<td><c:out value="${commodity.com_pic}" /></td>
							<td><c:out value="${commodity.com_size}" /></td>
							<td><c:out value="${commodity.com_saled_amount}" /></td>
							<td><c:out value="${commodity.com_color}" /></td>
							<td><c:out value="${commodity.com_rank_total}" /></td>
							<td><c:out value="${commodity.com_rank_num}" /></td>
							<td><c:out value="${commodity.if_deleted}" /></td>
							<td>
								<!-- add the update link --> <a href="${updateLink}">Edit</a>| <a
								href="${deleteLink}"
								onclick="if(!(confirm('Are you going to delete this infomation?'))) return false">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</table>
	
			</div>
			</main>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
	</script>
	<script src="../../assets/js/vendor/popper.min.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>

	<!-- Icons -->
	<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
	<script>
		feather.replace()
	</script>




</body>


</html>

