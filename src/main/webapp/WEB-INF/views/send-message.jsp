<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Send-Message</title>

    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">

    <style type="text/css">
        #abc {
            border: 0px;
            border-radius: 20px;
            background-color: "#000000";

            padding-left: 3px;
            padding-right: 3px;
            height: 128px;
            font-size: 18px;
            width: 100px;
        }

        div#container {
            margin: 18px auto;

            width: 1500px;
            height: 1100px;
        }

        div#header {

            border: 1pt solid #A6CFE1;
            width: 100%;
            height: 7%;
        }

        div#middle {
            margin: 18px auto;
            background-color: #F1F1F1;
            border: 1pt solid #A6CFE1;
            width: 95%;
            height: 1000px;
        }

        div#Left {
            width: 30%;
            padding: 23px 0px 0px 0px;
            height: 850px;

            float: left;
        }

        div#Right {
            width: 70%;
            padding: 23px 0px 0px 0px;
            height: 850px;

            float: left;
        }

        div#LeftTop {
          width: 85%;
          height: 35%;
          padding-left: 10px;
          background-color: #339EFF; 
          font-size: 18px;
          float: left;
        }

        div#LeftBottom {
          width: 85%;
         height: 70%;
         margin-left: 10px;
         padding-top: 50px;
         padding-left: 50px;
         font-size: 20px;
         background-color: #FFFFFF;  
          float: left;
        }

        div#RightOne {
          width: 80%;
          height: 82px;
         background-color: #339EFF; 
         margin-top: 50px;
         margin-left: 70px;
         padding-top: 30px;
         padding-left: 70px;
         font-size:25px;
         font-color:#ffffff;
         color:#ffffff;

        }

        div#RightTwo {
            width: 100%;
            height: 62px;

            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightThree {
            width: 100%;
            height: 62px;
            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightFour {
            width: 100%;
            height: 62px;
            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightFive {
            width: 100%;
            height: 250px;
            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightSix {
            width: 100%;
            height: 62px;

            padding-top: 220px;
            padding-left: 350px;

        }

        .SenderInfo {
            margin-top: 160px;
            text-align: center
        }

        .SenderName {
            font-size: 150%
        }

        .SenderEmail {

        }

    </style>
</head>
<body>

<div id="container">
    <div id="header">
        <a class="backHome btn btn-outline-primary" style="float: left;margin-top:18px;margin-left: 15px"
           href="/sydney/showHomePage">
            Home
        </a>
        <a class="Logout btn btn-outline-dark" style="float:right;margin-top:18px;margin-right:15px;" href="/sydney/logout">
            Log out
        </a>
    </div>
    <div id="middle">
        <div id="Left">
            <div id="LeftTop">
                <div class="SenderInfo">
                    <p class="SenderName">${messageSenderName}</p>
                    <p class="SenderEmail">@${userEmailOfSender}</p>
                </div>
            </div>
            <div id="LeftBottom">Items for you</div>
        </div>
        <div id="Right">
            <form:form action="sendNewMessage" method="POST">
                <div id="RightOne"> Send New Message</div>
                <div id="RightTwo">
                    <span>From : </span>
                    <span> ${messageSenderName} </span>
                </div>
                <div id="RightThree">
                    <span>Send To : </span>
                    <span>
                        <select class="custom-select" id="BBB2" name="sendtoname"
                                style="width: 200px; border-radius:10px; font-size:21px;">
                            <c:forEach items="${userList}" var="user">
                                <option value="${user.userName}">${user.userName}</option>
                            </c:forEach>
                        </select>
                    </span>
                </div>
                <div id="RightFour">
                    <span>Title : </span>
                    <span><input type="text" required name="messageTitle" id="BBB3" value="" style="border-radius:10px;font-size:21px;"> </span>
                </div>
                <div id="RightFive">
                    <textarea  name="messagecontent" required rows='10' cols='90' style="font-size:21px;"></textarea>
                </div>

                <div id="RightSix">
                    <input type="reset" value="Reset" href="/"
                           style="background-color:#cc0000; color: #ffffff; border-radius:10px;font-size:21px;"/>
                    <input type="submit" value="Submit"
                           style="background-color:#00ff00; color: #ffffff; border-radius:10px;font-size:21px;"/>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>

