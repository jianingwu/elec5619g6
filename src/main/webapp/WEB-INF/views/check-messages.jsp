<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Receive-Message</title>

    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
    <style type="text/css">
        #abc {
            border: 0px;
            border-radius: 20px;
            background-color: "#000000";

            padding-left: 3px;
            padding-right: 3px;
            height: 128px;
            font-size: 18px;
            width: 100px;
        }

        div#container {
            margin: 18px auto;

            width: 1500px;
            height: 1100px;
        }

        div#header {

            border: 1pt solid #A6CFE1;
            width: 100%;
            height: 7%;
        }

        div#middle {
            margin: 18px auto;
            background-color: #F1F1F1;
            border: 1pt solid #A6CFE1;
            width: 95%;
            height: 1000px;
        }

        div#Left {
            width: 30%;
            padding: 23px 0px 0px 0px;
            height: 850px;

            float: left;
        }

        div#Right {
            width: 70%;
            padding: 23px 0px 0px 0px;
            height: 850px;

            float: left;
        }

        div#LeftTop {
          width: 85%;
          height: 35%;
          padding-left: 10px;
          background-color: #339EFF; 
          font-size: 18px;
          float: left;
        }

        div#LeftBottom {
            width: 85%;
            height: 70%;
            margin-left: 10px;
            padding-top: 50px;
            padding-left: 50px;
            font-size: 20px;
            background-color: #FFFFFF;  
            float: left;
        }

        div#RightOne {
            width: 85%;
            height: 82px;
            background-color: #339EFF; 
            margin-top: 50px;
            margin-left: 70px;
            padding-top: 30px;
            padding-left: 70px;
            font-size:25px;
            font-color:#ffffff;
            color:#ffffff;

        }

        div.RightTwo {
            width: 100%;
            height: 62px;

            padding-top: 50px;
            padding-left: 50px;

        }

        div.RightThree {
            width: 100%;
            height: 62px;
            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightFour {
            width: 100%;
            height: 62px;
            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightFive {
            width: 100%;
            height: 250px;
            padding-top: 50px;
            padding-left: 50px;

        }

        div#RightSix {
            width: 100%;
            height: 62px;

            padding-top: 50px;
            padding-left: 350px;

        }

        .SenderInfo {
            margin-top: 160px;
            text-align: center
        }

        .SenderName {
            font-size: 150%
        }

        .SenderEmail {
        }

        .listsenderName {
            float: left;
            width: 10%;

        }
    </style>

</head>


<body>
<div id="container">
    <div id="header">
        <a class="backHome btn btn-outline-primary" style="float: left;margin-top:18px;margin-left: 15px"
           href="/sydney/showHomePage">
            Home
        </a>
        <a class="Logout btn btn-outline-dark" style="float:right;margin-top:18px;margin-right:15px;" href="/sydney/logout">
            Log out
        </a>
    </div>
    <div id="middle">
        <div id="Left">
            <div id="LeftTop">
                <div class="SenderInfo">
                    <p class="SenderName">${messageSenderName}</p>
                    <p class="SenderEmail">@${userEmailOfSender}</p>
                </div>
            </div>
            <div id="LeftBottom">Items for you</div>
        </div>
        <div id="Right">
            <form>
                <div id="RightOne"> Receive Message</div>
                <c:forEach items="${Messages}" var="message">
                    <div class="RightThree">
                        <div>
                            <p class="listsenderName">
                                    ${message.mgSenderName}:
                            </p>
                            <span>
                                <input type="text" name="BBB2" id="BBB${message.id}" disabled value="${message.mgTitle}"
                                       style="border-radius:10px; font-size:21px;margin-right:7%; float: right; width: 78%;">
                            </span>
                        </div>
                        <textarea name="messagecontent"
                                  style="width: 78%;margin-right:7%; float: right; margin-top: 20px" disabled
                                  id="messagecontent" cols="30" rows="4"
                                  class="form-control" required>${message.mgContent}</textarea><br/>
                        <div>
                                <%--<button onclick="javascript:deleteById('${message.id}')" class="btn btn-danger btn-sm" style="float: right;margin: 20px;">Delete</button>--%>
                            <a id="deleteButton"
                               style=" margin-left: 90%;margin-bottom:50px;margin-right:7%;float: right;margin-top:20px;"
                               class="btn btn-danger btn-sm"
                               href="checkmessages?id=${message.id}">
                                Delete
                            </a>
                        </div>
                    </div>
                </c:forEach>
            </form>
        </div>
    </div>
</div>

<script>
    function deleteById(sth) {
        window.location.href = "checkmessages?id=" + sth;
    }
</script>

</body>


<%--<body>--%>
<%--<div class="bg-light">--%>
<%--<div class="container">--%>
<%--<div class="row">--%>
<%--<div class="col-md-4 order-md-2 mb-4">--%>
<%--<h4 class="d-flex receiveBar justify-content-between align-items-center mb-3">--%>
<%--<span class="text-muted"></span>--%>
<%--</h4>--%>
<%--<div class="senderName text-center">--%>
<%--${messageSenderName}--%>
<%--</div>--%>
<%--<div class="emailOfSender text-center">--%>
<%--@${userEmailOfSender}--%>
<%--</div>--%>
<%--<ul class="list-group mb-3 text-center">--%>
<%--Items for you!--%>
<%--</ul>--%>
<%--</div>--%>
<%--<div class="col-md-8 order-md-1">--%>
<%--<h4 class="mb-3 receiveBar">Receive Message</h4>--%>
<%--<c:forEach items="${Messages}" var="message">--%>
<%--<div class="row">--%>
<%--<div class="col-md-3 text-center">--%>
<%--${message.mgSenderName}:--%>
<%--</div>--%>
<%--<div class="col-md-9">--%>
<%--<input type="text" disabled name="" class="inputControl" id=""--%>
<%--placeholder="${message.mgTitle}">--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="row">--%>
<%--<div class="col-md-3 text-center">--%>
<%--<button id="deleteButton" class="btn btn-danger"--%>
<%--onclick="javascript:deleteById('${message.id}')">--%>
<%--Delete--%>
<%--</button>--%>
<%--</div>--%>
<%--<div class="col-md-9">--%>
<%--<textarea name="messagecontent" disabled id="messagecontent" cols="30" rows="4"--%>
<%--class="form-control" required>${message.mgContent}</textarea>--%>
<%--</div>--%>
<%--</div>--%>
<%--</c:forEach>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--<script>--%>
<%--function deleteById(sth) {--%>
<%--window.location.href = "checkmessages?id=" + sth;--%>
<%--}--%>
<%--</script>--%>

<%--</body>--%>

</html>
