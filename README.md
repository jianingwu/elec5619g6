**Snapshop Project**

---

## Jianing Wu User Componet

In user component, several functions are realized

1. Register a new user
2. Log in with username
3. User role:
	a) add a new address
	b) edit address
	c) delete address
4.Log out
5.In userAPIController, it returns Json object with id, email and username

## Runze Zhu Shop Componet

In shop component, several functions are realized

1. commodity management
	a) add new commodity
	b) edit commodity
	c) delete commodity
2. shop page layout
3. business process
4. create order
5. use user API and display

## Qiushi Zhang Post Componet

In post component, several functions are realized

1. read post
2. publish post

## Jiajun Lin message Componet

In message component, several functions are realized

1. write message
2. check message
3. delete message
	a) add new commodity
	b) edit commodity
	c) delete commodity
---

## User SQL scripts

Just open mysql workbench and run it.

-- Table structure for table `user`
--
DROP SCHEMA IF EXISTS `elec5619`;

CREATE SCHEMA `elec5619`;

use `elec5619`;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) UNIQUE,
  `email` varchar(45) DEFAULT NULL,
  `password` char(68) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `user_detail`;

CREATE TABLE `user_detail`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) DEFAULT NULL,
	`first_name` varchar(45) DEFAULT NULL,
    `last_name` varchar(45) DEFAULT NULL,
	`email` varchar(45) DEFAULT NULL,
	`phone_number` varchar(45) DEFAULT NULL,
    `street` varchar(45) DEFAULT NULL,
    `postcode` int(11) DEFAULT NULL,
    `city` varchar(45) DEFAULT NULL,
	`suburb` varchar(45) DEFAULT NULL,
	`state` varchar(45) DEFAULT NULL,
    
    PRIMARY KEY (`id`),
    KEY `FK_USER_idX`(`user_id`),
    CONSTRAINT `FK_USER`
    FOREIGN KEY(`user_id`)
    REFERENCES `user` (`id`)
    
    ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

## Shop SQL scripts

use `elec5619`;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `commodity`;

CREATE TABLE `commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_name` varchar(45) DEFAULT NULL,
  `com_amount` int(10) DEFAULT 0,
  `com_price` float(11) DEFAULT 0,
  `com_discount` float(11) DEFAULT 1,
  `real_price` float(11) DEFAULT 1,
  `com_type` varchar(30) DEFAULT NULL,
  `com_describe` varchar(300) DEFAULT NULL,
  `com_pic` varchar(100) DEFAULT NULL,
  `com_size` char(10) DEFAULT NULL,
  `com_saled_amount` int(10) DEFAULT 0,
  `com_color` varchar(10) DEFAULT NULL,
  `com_rank_total` float(10) DEFAULT 0,
  `com_rank_num` int(10) DEFAULT 0,
  `if_deleted` bool DEFAULT false,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

use `elec5619`;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `snap_order`;

  CREATE TABLE `snap_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT 0,
  `commodity_id` int(11) DEFAULT 0,
  `com_amount` int(10) DEFAULT 0,
  `com_name` varchar(45) DEFAULT NULL,
  `price` float(11) DEFAULT 0,
  `total_price` float(11) DEFAULT 0,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `postcode` int(11) DEFAULT NULL,
  `order_time` varchar(30) DEFAULT NULL,
  `if_deleted` bool DEFAULT false,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


## post SQL scripts

use `elec5619`;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `snap_post`;

CREATE TABLE `snap_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT 0,
  `post_user_name` varchar(45) DEFAULT NULL,
  `post_pic1` varchar(100) DEFAULT NULL,
  `post_pic2` varchar(100) DEFAULT NULL,
  `post_pic3` varchar(100) DEFAULT NULL,
  `post_content` varchar(500) DEFAULT NULL,
  `post_like_amount` int(10) DEFAULT 0,
  `post_time` varchar(30) DEFAULT NULL,
  `if_deleted` bool DEFAULT false,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

## message SQL scripts

use elec5619;

create table messageTable (
id int(11)  NOT NULL AUTO_INCREMENT,
mgSenderId int(11),
mgReceiverId int(11),
mgTitle varchar(80),
mgContent varchar(800),
mgSenderName varchar(80),
mgReceiverName varchar(80),
PRIMARY KEY (id) )  ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;